import { Component } from "react";

class TimeClick extends Component{
    constructor(props) {
        super(props)
        this.state = {
            date: []
        }
    }
    ButtonClickHandler = () => {
        const dateAdd = new Date().toLocaleTimeString();
        this.setState({
            date: [...this.state.date, dateAdd]
        })
    }

    render() {
        const dataUpdate = this.state.date;
        return (
            <div>
                <ul>List:
                    {dataUpdate.map((data,index) => 
                        <li key={index}>{data}</li>
                    )}
                </ul>
                <button onClick={this.ButtonClickHandler}>Add to List</button>
            </div>
        )
    }
}

export default TimeClick;